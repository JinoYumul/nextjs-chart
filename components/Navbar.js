import Navbar from "react-bootstrap/Navbar";
import Link from "next/link";

export default function Navibar() {
  return (
    <Navbar bg="light" expands="lg">
      {" "}
      <Link href="/covid">
        <a className="navbar-brand">Covid-19 Tracker</a>
      </Link>
      <Navbar.Toggle />
      <Navbar.Collapse>
        <Link href="/covid/countries">
          <a className="nav-link">Infected Countries</a>
        </Link>
        <Link href="/covid/search">
          <a className="nav-link">Find Countries</a>
        </Link>
        <Link href="/covid/top">
          <a className="nav-link">Top 10 Countries</a>
        </Link>
      </Navbar.Collapse>
    </Navbar>
  );
}
