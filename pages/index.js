import Jumbotron from "react-bootstrap/Jumbotron";
import toNum from "../helpers/toNum";

export default function Home({ data, total }) {
  console.log(data);
  return (
    <Jumbotron>
      <h1>There are {total} cases globally</h1>
    </Jumbotron>
  );
}

export async function getServerSideProps() {
  const res = await fetch(
    "https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php",
    {
      method: "GET",
      headers: {
        "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
        "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
      },
    }
  );

  const data = await res.json();
  const countriesStats = data.countries_stat;

  let total = 0;

  countriesStats.forEach((country) => {
    total += toNum(country.cases);
  });

  return {
    props: {
      data,
      total,
    },
  };
}
