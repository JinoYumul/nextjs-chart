import Head from "next/head";

export default function index() {
  return (
    <>
      <Head>
        <title>Top Page</title>
      </Head>
      <div>
        <h1>Hello from Top page</h1>
      </div>
    </>
  );
}
