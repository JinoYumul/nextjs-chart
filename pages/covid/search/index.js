import Head from "next/head";

export default function index() {
  return (
    <>
      <Head>
        <title>Search Page</title>
      </Head>
      <div>
        <h1>Hello from Search page</h1>
      </div>
    </>
  );
}
