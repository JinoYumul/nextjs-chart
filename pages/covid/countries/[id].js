import Jumbotron from "react-bootstrap/Jumbotron";
import Button from "react-bootstrap/Button";
import Link from "next/link";
import DoughnutChart from "../../../components/DoughnutChart";
import toNum from "../../../helpers/toNum";

export default function id({ country }) {
  return (
    <div>
      <Jumbotron>
        <h1>Name: {country.country_name}</h1>
        <h3>Deaths: {country.deaths}</h3>
        <h3>Serious Critical: {country.serious_critical}</h3>
        <h3>Total Recovered: {country.total_recovered}</h3>
        <Button variant="info">
          <Link href="/covid/countries">
            <a>Back</a>
          </Link>
        </Button>
      </Jumbotron>
      <DoughnutChart
        criticals={toNum(country.serious_critical)}
        deaths={toNum(country.deaths)}
        recoveries={toNum(country.total_recovered)}
      ></DoughnutChart>
    </div>
  );
}

export async function getStaticPaths() {
  const res = await fetch(
    "https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php",
    {
      method: "GET",
      headers: {
        "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
        "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
      },
    }
  );

  const data = await res.json();

  const paths = data.countries_stat.map((country) => ({
    params: {
      id: country.country_name,
    },
  }));

  return { paths, fallback: false };
}

export async function getStaticProps({ params }) {
  const res = await fetch(
    "https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php",
    {
      method: "GET",
      headers: {
        "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
        "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
      },
    }
  );

  const data = await res.json();
  const country = data.countries_stat.find(
    (country) => country.country_name === params.id
  );

  return {
    props: {
      country,
    },
  };
}
