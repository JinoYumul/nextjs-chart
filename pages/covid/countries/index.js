import Head from "next/head";
import Link from "next/Link";
import ListGroup from "react-bootstrap/ListGroup";

export default function index({ data }) {
  const countryList = data.countries_stat.map((country) => {
    return (
      <ListGroup.Item key={country.country_name}>
        <Link href={`/covid/countries/${country.country_name}`}>
          <a>{country.country_name}</a>
        </Link>
      </ListGroup.Item>
    );
  });
  return (
    <>
      <Head>
        <title>Countries Page</title>
      </Head>
      <div>{countryList}</div>
    </>
  );
}

export async function getStaticProps() {
  const res = await fetch(
    "https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php",
    {
      method: "GET",
      headers: {
        "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
        "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
      },
    }
  );

  const data = await res.json();

  return {
    props: {
      data,
    },
  };
}
