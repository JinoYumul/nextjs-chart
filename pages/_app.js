import "bootstrap/dist/css/bootstrap.min.css";
import Navibar from "../components/Navbar";
import { Container } from "react-bootstrap";

export default function App({ Component, pageProps }) {
  return (
    <>
      <Navibar />
      <Container>
        <Component {...pageProps} />
      </Container>
    </>
  );
}
